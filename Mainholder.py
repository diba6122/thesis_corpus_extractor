'''
Created on Jun 19, 2013

@author: M.S.D
'''
import os 
import sys
import re
import time
import AlchemyAPI
import subprocess
from xml.dom.minidom import parseString
from _mysql_exceptions import Error
import pyipinfodb
import urllib2
#import MySQLdb
import codecs
class Mainholder():
    def extract_simple_facets(self,emailtype):
        ''' global variables '''
        alchemyObj = AlchemyAPI.AlchemyAPI()
        alchemyObj.loadAPIKey("api_key.txt");
        output_csvfile_name = str(emailtype)+"_output.csv"
        output  = open(output_csvfile_name,'w')
        iplocator= pyipinfodb.IPInfo("ba0f471b95215cc1dc7166527ef8de41f1ecf1dd10fa9782c9ae93654f336ffd")
        ipregex = re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
        path =  "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/"+emailtype.upper()
        html_path =  "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HTMLOUTPUT/"+emailtype.upper()
        list = []
        file_list = os.listdir(path)
        count = 0
        ''' End of global variables  '''


        for file_name in file_list:
            count+=1
            
            facets  = {"id":"","from":"","to":"","message-id":"","subject":"","city":'','country':'','type':'','attachment':'','cc':'','http':'','https':'','image':'','charset':'','date':'','size':'','noofline':'','dayofweek':'','category':'','messagebody':'','html_file':''}
            
            ''' Reading input email header, line by line pass it to a list of lines
                also pass the whole file to a string variable            '''
            
            file = open(os.path.join(path,file_name),'r')
            line_list = file.readlines()
            file.close()
            file = open(os.path.join(path,file_name),'r')
            alllines= file.read()
            file.close()
            
            ''' End of reading input file '''

            ''' Extracting all available IP addresses in a message header
                using a regular expression and defining a list called city    '''

            iplist = ipregex.findall(alllines)    
            city = []
            for ip in iplist:
                ip_dic = {'city':'','country':''}
                ipinfo = iplocator.GetCity(ip)
                ip_dic.update({'city':ipinfo.get('City'),'country':ipinfo.get('CountryName')})
                city.append(ip_dic)


            ''' End of getting city and country related to each IP address in an Email header, 
                            a dictionary for each IP address is appended to city list   '''

            ''' Checking the email header to find simple facets '''
            
            facets.update({'id':str(file_name)})                 
            facets.update({'type':emailtype.lower()}) 

            if (alllines.lower().find('attach')!=-1):
                facets.update({'attachment':'TRUE'})
            else:
                facets.update({'attachment':'FALSE'})
            if (alllines.lower().find('http:')!=-1):
                facets.update({'http':'TRUE'})
            else:
                facets.update({'http':'FALSE'})
            if (alllines.lower().find('https:')!=-1):
                facets.update({'https':'TRUE'})
            else:
                facets.update({'https':'FALSE'})
            if (alllines.lower().find('<img ')!=-1):
                facets.update({'image':'TRUE'})
            else:
                facets.update({'image':'FALSE'})

            ''' End of simple facet extraction '''

            for line in line_list:
                html_start_index=-1
                if line.lower().find('from:')!=-1:
                    sender =  re.findall(r'[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+',line)
                    try:
                        facets.update({'from':str(sender[0]).replace('\n', '')})
                    except IndexError:
                        facets.update({'from':str(sender).replace('\n', '')})
                elif line.lower().find('to:')!=-1:
                    reciever =  re.findall(r'[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+',line)
                    try:
                        facets.update({'to':str(reciever[0]).replace('\n', '')})
                    except IndexError:
                        facets.update({'to':str(reciever).replace('\n', '')})
                elif line.lower().find('message-id:')!=-1:
                    try:
                        messageid = line.split('<')[1].replace('>','')
                    except IndexError:
                        messageid = line.lower().replace('message-id:','')
                    facets.update({'message-id':str(messageid).replace('\n', '')})
                elif line.lower().find('subject:')!=-1:
                    subject= line[8:len(line)]
                    facets.update({'subject':str(subject).replace('\n', '')})
                elif line.lower().find('cc:')!=-1:
                    try:
                        facets.update({'cc':line.split('<')[1].replace('>','')})
                    except IndexError:
                        facets.update({'cc':str(line).lower().replace('cc', '').replace('\n', '')})
                elif line.lower().find('charset=')!=-1:
                    try:
                        facets.update({'charset':str(line.split('charset=')[1].split('"')[0].replace('=','')).replace('\n', '')})
                    except IndexError:
                        facets.update({'charset':str(line).lower().replace('charset=', '').replace('\n', '').split('"')[0].replace('=','')})
                elif line.lower().find('date:')!=-1:
                    dayofweek = '' 
                    date =''
                    line = line.lower().replace('date:','')
                    try:
                        if(len(line.split(","))==2):
                            dayofweek= line.split(',')[0].replace(' ','')
                            date =line.split(',')[1]
                        else:
                            dayofweek= ''
                            date =line.split(',')[0]
                        if(date.find(' ')==0):
                            date=date[1:len(date)]
                        if(date.find('-')!=-1):
                            date=date[0:date.find('-')-1]
                        if(date.find('+')!=-1):
                            date=date[0:date.find('+')-1]
                        facets.update({'date':str(date).replace('\n', ''),'dayofweek':str(dayofweek).replace('\n', '')})
                    except IndexError:
                        dayofweek= line.split(',')[0].replace(' ','')
                        date =line.split(',')[1] 
                        facets.update({'date':str(date).replace('\n', ''),'dayofweek':str(dayofweek).replace('\n', '')})
                elif line.lower().find('content-length:')!=-1:
                    try: 
                        facets.update({'size':line.lower().split(':')[1].replace(' ','').replace('\n','')})
                    except IndexError:
                        facets.update({'size':line.lower().split(':').replace(' ','').replace('\n','')})
                elif line.lower().find('lines:')!=-1:
                    try: 
                        facets.update({'noofline':line.lower().split(':')[1].replace(' ','').replace('\n','')})
                    except IndexError:
                        facets.update({'noofline':line.lower().split(':').replace(' ','').replace('\n','')})
                elif line.lower().find('<html>')!=-1:
                    html_start_index = line_list.index(line)
                    if(html_start_index != -1):
                        html_body = ''
                        for i in range(html_start_index,len(line_list)):
                            html_body = html_body+line_list[i]
                        output_html_file_name= str(file_name).replace('.','')+'.html'
                        output_html_file = open(os.path.join(html_path,output_html_file_name),'w')
                        output_html_file.write(html_body)
                        output_html_file.close()
                        facets.update({'html_file':output_html_file_name})                               #update the html_file path <key,valu> facet information 
                        result = ''
                        try:
                            result = alchemyObj.HTMLGetRawText(str(html_body).replace(',',''),"http://www.test.com/")
                        except Exception, err:
                            facets.update({'messagebody':''}) 
                            sys.stderr.write('GetRawText ERROR in: %s\n' % str(err))   
                        if result!='':
                            dom = parseString(result)
                            xmlTag = ''
                            try:
                                xmlTag = dom.getElementsByTagName('text')[0].toxml()
                            except IndexError:
                                xmlTag = ''
                            if(xmlTag!=''):
                                html_text = xmlTag.replace('<text>','').replace('</text>','')
                                html_text = html_text.encode("ascii","ignore")
                                facets.update({'messagebody':str(html_text).replace(',','').replace('\n','')})     # update the messagebody  <key,valu> facet information 
                                try:
                                    category= alchemyObj.TextGetCategory(html_text)
                                    dom  = parseString(category)
                                    xmlTag =''
                                    try:
                                        xmlTag = dom.getElementsByTagName('category')[0].toxml()
                                    except IndexError:
                                        xmlTag = ''
                                    if(xmlTag==''):
                                        m=''
                                    else:
                                        category_text = xmlTag.replace('<category>','').replace('</category>','')
                                        category_text = category_text.encode('ascii','ignore')
                                        print category_text
                                        time.sleep(20)
                                        facets.update({'category':str(category_text).replace(',','').replace('\n','')})      #update the category  <key,valu> facet information 
                                except Exception, err:
                                    sys.stderr.write('AlchemyGetCategory ERROR in: %s\n' % str(err))
                                    facets.update({'category':'unknown'})
                        else:
                             facets.update({'messagebody':''}) 
                             facets.update({'category':'unknown'})
            for citydic in city:
                facets.update({'city':str(citydic.get('city').encode("ascii","ignore")).replace(',',''),'country':str(citydic.get('country').encode("ascii","ignore")).replace(',','')})
                string = Mainholder().buildOutputString(facets)
                try:
                    output.write(string)
                except UnicodeDecodeError:
                    pass
            print str(count) +" file are processed." +str(file_name)
        output.close()   
        return 0
    def buildOutputString(self,facets):
        string  = ''
        if(facets.get('from')!=''):
            string+=str(facets.get('from')).replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('to')!=''):
            string+=str(facets.get('to')).replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('message-id')!=''):
            string+=facets.get('message-id').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('subject')!=''):
            string+=facets.get('subject').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('city')!=''):
            string+=str(facets.get('city')).replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('country')!=''):
            string+=str(facets.get('country')).replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('type')!=''):
            string+=facets.get('type').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('attachment')!=''):
            string+=facets.get('attachment').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('cc')!=''):
            string+=facets.get('cc').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('http')!=''):
            string+=facets.get('http').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('https')!=''):
            string+=facets.get('https').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('image')!=''):
            string+=facets.get('image').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('charset')!=''):
            string+=facets.get('charset').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('date')!=''):
            string+=facets.get('date').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('size')!=''):
            string+=facets.get('size').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('noofline')!=''):
            string+=facets.get('noofline').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('dayofweek')!=''):
            string+=facets.get('dayofweek').replace('\n','').replace(',',' ').replace(" ",'')+","
        else:
            string+=''+','
        if(facets.get('category')!=''):
            string+=facets.get('category').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('messagebody')!=''):
            string+=facets.get('messagebody').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('html_file')!=''):
            string+=facets.get('html_file').replace('\n','').replace(',',' ')+","
        else:
            string+=''+','
        if(facets.get('id')!=''):
            string+=facets.get('id').replace('\n','').replace(',',' ')+"\n"
        else:
            string+=''+"\n"
        return string
    def blacklistcheck(self):
        #output_csvfile_name = str(emailtype)+"_output.csv"
        #output  = open(output_csvfile_name,'w')
        path =  "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HAM"
        outputpath  = "/Users/sina/Desktop/ham_blacklisted.csv"
        pattern = re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
        spamdbs = ['.dnsbl.sorbs.net', '.cbl.abuseat.org', '.bl.spamcop.net', '.zen.spamhaus.org', '.sbl.spamhaus.org', '.xbl.spamhaus.org', '.pbl.spamhaus.org']
        outputfile = open(outputpath,'w')
        file_list = os.listdir(path)
        ''' End of global variables  '''
        blacklistedfile = {}
        for file_name in file_list:
            blacklistedfile.update({str(file_name):"FALSE"})
            file = open(os.path.join(path,file_name),'r')
            alllines= file.read()
            file.close()
            iplist = pattern.findall(alllines)   
            for address in iplist:
               # print "checking for %s , in %s file"%(str(address),str(file_name))
                ip = str(address).split(".")
                rev = "%s.%s.%s.%s" % (ip[3],ip[2],ip[1],ip[0])
                blacklisted = False
                for db in spamdbs:
                    p = subprocess.Popen(["dig", "+short", rev+db], stdout=subprocess.PIPE)
                    output, err = p.communicate()
                    if(str(output).find("127.0.0")!=-1):
                        blacklisted= True
                        break
                if blacklisted:
                    blacklistedfile.update({str(file_name):"TRUE"})
                    break
        for email in blacklistedfile:
            outputfile.write(str(email)+","+blacklistedfile.get(email)+"\n")
    def language_check(self):
        #output_csvfile_name = str(emailtype)+"_output.csv"
        #output  = open(output_csvfile_name,'w')
        alchemyObj = AlchemyAPI.AlchemyAPI()
        alchemyObj.loadAPIKey("api_key.txt");
        path =  "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HTMLOUTPUT/SPAM"
        outputpath  = "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HTMLOUTPUT/spam_language.csv"
        outputfile = open(outputpath,'w')
        file_list = os.listdir(path)
        language_dic = {}
        for file_name in file_list:
            if(file_name.find('.html')!=-1):
                language_dic.update({str(file_name).replace('.html',''):'unknown'})
                file_path = os.path.join(path,file_name)
                file = open(file_path,'r')
                alllines= file.read()
                file.close()
                result = ''
                try:
                    result = alchemyObj.HTMLGetRawText(str(alllines).replace(',',''),"http://www.test.com/")
                except Exception, err:
                    sys.stderr.write('GetRawText ERROR in: %s\n' % str(err))   
                if result!='':
                    dom = parseString(result)
                    xmlTag = ''
                    try:
                        xmlTag = dom.getElementsByTagName('text')[0].toxml()
                    except IndexError:
                        xmlTag = ''
                    if(xmlTag!=''):
                        html_text = xmlTag.replace('<text>','').replace('</text>','')
                        html_text = html_text.encode("utf-8","ignore")
                        try:
                            result = alchemyObj.TextGetLanguage(html_text);
                            dom = parseString(result)
                            xmlTag = ''
                            try:
                                xmlTag = dom.getElementsByTagName('language')[0].toxml()
                            except IndexError:
                                xmlTag = ''
                            if(xmlTag!=''):
                                result = xmlTag.replace('<language>','').replace('</language>','')
                                result = result.encode("ascii","ignore")
                                language_dic.update({str(file_name).replace('.html',''):result})
                                if(result.lower()!='english'):
                                    print result+'\n'
                        except Exception, err:
                            sys.stderr.write('AlchemyGetCategory ERROR in: %s\n' % str(err))
        for email in language_dic:
            outputfile.write(str(email)+","+language_dic.get(email)+"\n")
        outputfile.close()
    def add_blacklist_facet(self):
        corpus_path  = "/Users/sina/Desktop/DATASET/data/sets/finalcorpus.csv" 
        outputcorpus = "/Users/sina/Desktop/DATASET/data/sets/finalcorpus1.csv"
        spam_blacklist = "/Users/sina/Desktop/spam_blacklisted.csv"
        ham_blacklist = "/Users/sina/Desktop/ham_blacklisted.csv" 
        
        outputcorpus_file= open(outputcorpus,'w')
        corpus = open(corpus_path,'r')

        corpus_data = corpus.read().split('\r')

        ham_blacklisted_file = open(ham_blacklist,'r')
        blacklist_ham  =  ham_blacklisted_file.readlines()
        ham_blacklisted_file.close()

        spam_blacklisted_file = open(spam_blacklist,'r')
        blacklist_spam = spam_blacklisted_file.readlines() 
        spam_blacklisted_file.close()

        for line in blacklist_ham: 
            data = line.split(',')
            blacklist_dic.update({str(data[0]).replace('\n',''):str(data[1]).replace('\n','')})
        print "len of blacklist_ham: %s \n"%(str(len(blacklist_dic)))
        for line in blacklist_spam: 
            data = line.split(',')
            blacklist_dic.update({str(data[0]).replace('\n',''):str(data[1]).replace('\n','')})
        print "len of blacklist_ham and blacklist_spam together: %s \n"%(str(len(blacklist_dic)))

        print "attaching facet to corpus \n"

        for line in corpus_data: 
            n = blacklist_dic.get(str(line.split(',')[20]).replace('\n','').replace('\r',''))
            if n==None:
                print "file %s not found in dictionary\n"%str(line.split(',')[20])
            else:
                line =  line.replace('\n','')+','+str(n)+"\n"
                print line+"\n" 
                outputcorpus_file.write(line)
        outputcorpus_file.close()
        print "Done!"
    def add_language_facet(self):
        corpus_path  = "/Users/sina/Desktop/DATASET/data/sets/finalcorpus.csv" 
        outputcorpus = "/Users/sina/Desktop/DATASET/data/sets/finalcorpus_language.csv"
        spam_langlist = "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HTMLOUTPUT/spam_language.csv"
        ham_langlist = "/Users/sina/Dropbox/OU-2013/Thesis/Corpus/trec07-html/Final/HTMLOUTPUT/ham_language.csv" 
        language_dic = {} 
        outputcorpus_file= open(outputcorpus,'w')
        corpus = open(corpus_path,'r')

        corpus_data = corpus.read().split('\r')

        ham_langlisted_file = open(ham_langlist,'r')
        langlisted_ham  =  ham_langlisted_file.readlines()
        ham_langlisted_file.close()

        spam_langlisted_file = open(spam_langlist,'r')
        langlisted_spam = spam_langlisted_file.readlines() 
        spam_langlisted_file.close()

        for line in langlisted_ham: 
            data = line.split(',')
            name= "inmail."+str(data[0]).replace('\n','').split('inmail')[1]
            language_dic.update({name:str(data[1]).replace('\n','')})
        print "len of blacklist_ham: %s \n"%(str(len(language_dic)))
        for line in langlisted_spam: 
            data = line.split(',')
            name= "inmail."+str(data[0]).replace('\n','').split('inmail')[1]
            language_dic.update({name:str(data[1]).replace('\n','')})
        print "len of blacklist_ham and blacklist_spam together: %s \n"%(str(len(language_dic)))

        print "attaching facet to corpus \n"
        f=open('test.txt','w')
        f.write(str(language_dic))
        f.close()
        for line in corpus_data: 
            n = language_dic.get(str(line.split(',')[20]).replace('\n','').replace('\r',''))
            print n
            if n==None:
                print "file %s not found in dictionary\n"%str(line.split(',')[20])
            else:
                line =  line.replace('\n','')+','+str(n)+"\n"
                outputcorpus_file.write(line)
        outputcorpus_file.close()
        print "Done!"









