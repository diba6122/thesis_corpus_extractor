'''
Created on Jun 19, 2013

@author: M.S.D
'''
import os 
import sys
import re
import time
import AlchemyAPI
import subprocess
from xml.dom.minidom import parseString
from _mysql_exceptions import Error
import pyipinfodb
import urllib2
#import MySQLdb
import codecs


alchemyObj = AlchemyAPI.AlchemyAPI()
alchemyObj.loadAPIKey("api_key.txt");
file  = open('index.html','r') 

htmlFileHandle = open("index.html", 'r')
htmlFile = htmlFileHandle.read()
htmlFileHandle.close()
result= ''
try:
    result = alchemyObj.HTMLGetRawText(str(htmlFile).replace(',',''),"http://www.test.com/")
except Exception, err:
    sys.stderr.write('GetRawText ERROR in: %s\n' % str(err))   
if result!='':
    dom = parseString(result)
    xmlTag = ''
    try:
        xmlTag = dom.getElementsByTagName('text')[0].toxml()
    except IndexError:
        xmlTag = ''
    if(xmlTag!=''):
        html_text = xmlTag.replace('<text>','').replace('</text>','')
        html_text = html_text.encode("utf-8","ignore")
        try:
            result = alchemyObj.TextGetLanguage(html_text);
            dom = parseString(result)
            xmlTag = ''
            try:
                xmlTag = dom.getElementsByTagName('language')[0].toxml()
            except IndexError:
                xmlTag = ''
            if(xmlTag!=''):
                result = xmlTag.replace('<language>','').replace('</language>','')
                result = result.encode("ascii","ignore")
            print result
        except Exception, err:
            sys.stderr.write('AlchemyGetCategory ERROR in: %s\n' % str(err))
